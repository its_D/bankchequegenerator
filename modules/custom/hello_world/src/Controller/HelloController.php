<?php

namespace Drupal\hello_world\Controller;

class HelloController{
/**
 * Displays Welcome message to the User
 *
 */
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('Hello, World!'),
    );
  }

}