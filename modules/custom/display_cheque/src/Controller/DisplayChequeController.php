<?php 

namespace Drupal\display_cheque\Controller;
use TNkemdilim\MoneyToWords\Converter;
/**
 * Class to Display Cheque From the Form post data
 * 
 */

class DisplayChequeController{

    public function display(){

        $user = $_SESSION['user']; //Mock Fetch from database

        $user['sum_cheque_format'] = $this->convert_to_currency($user['sum']);
        $user['sum'] = $this->numeric_sum($user['sum']);
        return array(
            '#theme' => 'display-cheque-theme-hook',
            '#user' => $user,
            '#title' => 'Cheque'
        );
    }

    /**
     * Converts the sum to numeric values
     * @param int sum
     * @return string numeric sum
     */
    private function numeric_sum($num){
        return number_format( $num, $decimals = 2, $decimal_separator = ".", $thousands_separator = ",");
    }
  
    /**
     * Convert sum to cheque writing format 
     * @param int sum
     * @return string cheque writing format
     */
    private function convert_to_currency($num){
        $converter = new Converter('dollar', 'cents'); // can make the parameters dynamic based on the application needs
        return $converter->convert($num);
    }
}
?>