<?php 
namespace Drupal\cheque_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Cheque Form Class
 * 
 */
class ChequeForm extends FormBase{

    /**
     * Return the form Id
     */
    public function getFormId(){
        return 'cheque_form';
    }

    /**
     * Builds the form and return 
     * the form
     */
    public function buildForm(array $form, FormStateInterface $form_state){
        
        $form['first_name'] = [
            '#type' => 'textfield',
            '#title' => t('First Name'),
            '#required' => TRUE
        ];

        $form['last_name'] = [
            '#type' => 'textfield',
            '#title' => t('Last Name'),
            '#required' => TRUE
        ];

        $form['payee_name'] = [
            '#type' => 'textfield',
            '#title' => t('Payee Name'),
            '#required' => TRUE
        ];

        $form['sum'] = [
            '#type' => 'number',
            '#title' => t('Sum'),
            '#required' => TRUE
        ];

        $form['actions']['#type'] = 'actions';
        
        $form['actions']['submit'] = [
			'#type' => 'submit',
			'#value' => t('Submit'),
			'#button_type' => 'primary',
		];

        return $form;
    }
    
    /**
     * Submits the form
     */
    public function submitForm(array &$form, FormStateInterface $form_state){
        //Redirect User to the Cheque View
        $form_data = array(
            'first_name' => $form_state->getValue('first_name'),
            'last_name' => $form_state->getValue('last_name'),
            'payee_name' => $form_state->getValue('payee_name'),
            'sum' => $form_state->getValue('sum'),
            'date' => date('j F, Y')
        );
        $_SESSION['user'] = $form_data; // storing it in the session variable -> database mock
        $form_state->setRedirect('display_cheque.display_cheque');
    }

     /**
     * Validate the submitted Form
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

        $sum = $form_state->getValue('sum');

        $string_fields = array(
            'first_name' => $form_state->getValue('first_name'),
            'last_name' => $form_state->getValue('last_name'),
            'payee_name' => $form_state->getValue('payee_name')
        );

        foreach ($string_fields as $key => $input){
        
            if(!$this->isValid($input)){

                $form_state->setErrorByName($key, 'The text input should only contain alphabets');
            }
        } 
        if(!$this->isValid($sum, 'numeric')){
            $form_state->setErrorByName('sum', 'Invalid Number');
        }
      }

    /**
     * Checks if the input value is valid
     * @param string value
     * @param string numeric or string
     * @return boolean
     */
    public function isValid($value, $type = 'string'){

        if($value){

            if($type == 'string'){
                return is_string($value) && !(1 === preg_match('~[0-9]~', $value));
            }
            if($type == 'numeric'){
                return is_numeric($value) && $value > 0;
            }
        }
        return FALSE;
    }
}