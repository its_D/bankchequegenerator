Bank Cheque Generator

setup
Pull the repository

if using xampp

git clone it to xampp/htdocs/
and cd into it
run composer update to install dependencies

Then head to localhost/project_you_pulled to access the site Follow the installation

head to sites\default\settings.php and uncomment the following line

if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) { include $app_root . '/' . $site_path . '/settings.local.php'; } This will give settings.local precedence

Copy the example.settings.local.php and rename it settings.local.php and set your configuration i.e database etc 

$databases['default']['default'] = array ( 'database' => 'bank_cheque_generator', 'username' => 'root', 'password' => '', 'prefix' => '', 'host' => 'localhost', 'port' => '3306', 'namespace' => 'Drupal\Core\Database\Driver\mysql', 'driver' => 'mysql', ); 

if running on a local server follwing setting will add local host as trusted 

$settings['trusted_host_patterns'] = [ '^localhost$',
'^192.168.00.52$', '^127.0.0.1$', ];

Configure your site to this uuid 'ce3d8fed-4a74-444f-b10d-2cf2a01356fa'

Find the file in the root of the repo  

config-localhost-2021-07-02-02-41.tar.gz 

head to (admin/config/development/configuration) and import the file

and the site should be in sync
The database file is included 

The custom modules created are

cheque_form
display_cheque

For Appearance
Custom Barrio 5.5.1 (default theme) was used // Bootstrap

Thank you.



